<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DS Advance - Mugs</title>
		<!-- CSS -->

		<!-- plugins -->
		<link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Josefin+Sans|Shadows+Into+Light|Pacifico|Quicksand|Architects+Daughter|Dancing+Script|Chewy|Gloria+Hallelujah|Shadows+Into+Light|Poiret+One|Indie+Flower|Oswald|Trebuchet+MS|Russo+One|Black+Ops+One|Kaushan+Script' rel='stylesheet' type='text/css'>
	
		<link rel="stylesheet"  href="assets/lib/nouislider/nouislider.min.css" media="all" type="text/css" />
		<!--/ plugins -->
		
		<link rel="stylesheet" href="assets/css/menu.css">
		<link rel="stylesheet" href="assets/css/media.css">
		<link rel="stylesheet" href="assets/css/index.css">
		<!--/ CSS -->
	</head>
	<body>
		<div id="loader">
			<p class="text-center"><br>Loading.... Please wait...</p>
		</div>
		<!-- header -->
		<header>
			<!-- Nav -->
			<nav class="animate focus">
				<ul class="nav nav-pills nav-stacked tool-options" role="tablist">
					<li role="presentation" class="active"><a class="text-center" title="Products" data-toggle="tab" aria-controls="tabpane-products" href="#tabpane-products"><i class="fa fa-th"></i></a></li>
					<li role="presentation"><a class="text-center" title="Text Options" data-toggle="tab" aria-controls="tabpane-text-options" href="#tabpane-text-options"><i class="fa fa-pencil"></i></a></li>
					<li role="presentation"><a class="text-center" title="Image Options" data-toggle="tab" aria-controls="tabpane-image-options" href="#tabpane-image-options"><i class="fa fa-picture-o"></i></a></li>
					<li role="presentation"><a class="text-center" title="Price Calculation" data-toggle="tab" aria-controls="tabpane-price-calculation-options" href="#tabpane-price-calculation-options"><i class="fa fa-cart-plus"></i></a></li>
				</ul>
			</nav>
			<!--/ Nav -->
			<div class="nav-controller hidden">
				<span class="[ ion-2x ion-navicon-round ] controller-open"></span>
				<span class="[ ion-2x ion-ios-close-outline ] controller-close"></span>
			</div>
		</header>
		<!--/ header -->		

		<!--  Main Container -->
		<div class="content-wrapper container-fluid">
			<div class="row">	

				<!-- Tools -->
				<div class="row tools">
					<div class="col-sm-8 col-md-10 col-sm-offset-10 col-md-offset-10">
						<div class="btn-group btn-group-justified" role="group" aria-label="">
							<div class="btn-group" role="group">
								<button title="Reset" type="button" data-toggle="confirmation" class="btn btn-default" id="reset"><i class="ion-2x ion-ios-loop"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Flip Horizontally" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Horizontally" class="btn btn-default" id="flipX"><i class="ion-2x ion-ios-arrow-thin-right"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Flip Vertically" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Vertically" class="btn btn-default" id="flipY"><i class="ion-2x ion-ios-arrow-thin-up"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Save" type="button" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-default" id="saveProduct"><i class="ion-2x ion-ios-heart-outline"></i></i></button>
							</div>
						</div>
					</div>
				</div>
				<!--/ Tools -->

				<!-- Design Tool -->
				<div class="row">

					<!-- Left -->
					<div class="col-sm-10 col-md-10">
						<div class="col-sm-18 col-md-12">
							<!-- Tab Panes -->
							<div class="tab-content tool-panes">
								<!-- Products -->
								<div role="tabpanel" class="products tab-pane fade in active" id="tabpane-products">
									<p class="custom-pane-header text-center h3">Products</p><hr>
									<div class="row display-gallery products-list">
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-01/mug-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-01/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-01/partials/left-base.png\", \"x\": 129, \"y\": 86, \"z\": 1,\"color\": \"#FED348\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-01/partials/left-handle.png\", \"x\": 47, \"y\": 104, \"z\": 2,\"color\": \"#D1316F\"}]","template":[{"type": "text", "params": {"src": "N", "editorMode": true, "draggable": true, "x": 135, "y": 144, "textAlign": "center", "font": "Kaushan Script", "colors": ["#D1316F"], "textSize": 120}},{"type": "text", "params": {"src": "IS", "editorMode": true, "draggable": true, "x": 253, "y": 157, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "FOR", "editorMode": true, "draggable": true, "x": 243, "y": 180, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "MR", "editorMode": true, "draggable": true, "x": 239, "y": 221, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "icole", "editorMode": true, "draggable": true, "x": 235, "y": 243, "textAlign": "center", "font": "Kaushan Script","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "image", "params": {"src": "assets/img/sample/decoration.png","editorMode": true, "draggable": true, "resizeToW": 209, "x": 134, "y": 106,"z":3}},{"type": "image", "params": {"src": "assets/img/sample/decoration.png","editorMode": true, "draggable": true, "resizeToW": 174, "x": 326, "y": 301,"degree":179}}]}'></div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-01/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-01/partials/right-base.png\", \"x\": 63, \"y\": 81, \"z\": 1,\"color\": \"#FED348\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-01/partials/right-handle.png\", \"x\": 268, \"y\": 99, \"z\": 2,\"color\": \"#D1316F\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/decoration.png","editorMode": true, "draggable": true, "resizeToW": 209, "x": 67, "y": 102}},{"type": "text", "params": {"src": "M", "editorMode": true, "draggable": true, "x": 78, "y": 140, "textAlign": "center", "font": "Kaushan Script", "colors": ["#D1316F"], "textSize": 120}},{"type": "text", "params": {"src": "IS", "editorMode": true, "draggable": true, "x": 218, "y": 154, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "FOR", "editorMode": true, "draggable": true, "x": 208, "y": 174, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "MRS", "editorMode": true, "draggable": true, "x": 200, "y": 208, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "ills", "editorMode": true, "draggable": true, "x": 201, "y": 231, "textAlign": "center", "font": "Kaushan Script","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "image", "params": {"src": "assets/img/sample/decoration.png","editorMode": true, "draggable": true, "resizeToW": 176, "x": 260, "y": 291,"degree":179}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-02/mug-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-02/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-02/partials/left-base.png\", \"x\": 76, \"y\": 92, \"z\": 1,\"color\": \"#E7DBCB\"}]","template":[{"type": "text", "params": {"src": "You are", "editorMode": true, "draggable": true, "x": 171, "y": 151, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#1E3C62"], "textSize": 25}},{"type": "text", "params": {"src": "the", "editorMode": true, "draggable": true, "x": 179, "y": 181, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#6D4D40"], "textSize": 18}},{"type": "text", "params": {"src": "Best", "editorMode": true, "draggable": true, "x": 216, "y": 178, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#1F9DA9"], "textSize": 25}},{"type": "text", "params": {"src": "MUM", "editorMode": true, "draggable": true, "x": 176, "y": 201, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#F58383"], "textSize": 38}},{"type": "image", "params": {"src": "assets/img/sample/flags.png","editorMode": true, "draggable": true, "resizeToW": 194, "x": 126, "y": 93}}]}'></div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-02/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-02/partials/right-base.png\", \"x\": 76, \"y\": 92, \"z\": 1,\"color\": \"#E7DBCB\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/flags.png","editorMode": true, "draggable": true, "resizeToW": 186, "x": 80, "y": 93}},{"type": "text", "params": {"src": "A   AZING", "editorMode": true, "draggable": true, "x": 122, "y": 150, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "M", "editorMode": true, "draggable": true, "x": 136, "y": 150, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "L   VELY", "editorMode": true, "draggable": true, "x": 122, "y": 170, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "O", "editorMode": true, "draggable": true, "x": 136, "y": 170, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "IN   ELLIJENT", "editorMode": true, "draggable": true, "x": 113, "y": 190, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "T", "editorMode": true, "draggable": true, "x": 136, "y": 190, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "H", "editorMode": true, "draggable": true, "x": 136, "y": 210, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "APPY", "editorMode": true, "draggable": true, "x": 151, "y": 210, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "B   AUTIFUL", "editorMode": true, "draggable": true, "x": 119, "y": 230, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "E", "editorMode": true, "draggable": true, "x": 136, "y": 230, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "MA   VELOUS", "editorMode": true, "draggable": true, "x": 105, "y": 250, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "R", "editorMode": true, "draggable": true, "x": 137, "y": 250, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-03/mug-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-03/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-03/partials/left-base.png\", \"x\": 138, \"y\": 88, \"z\": 1,\"color\": \"#85A35D\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-03/partials/left-handle.png\", \"x\": 71, \"y\": 93, \"z\": 2,\"color\": \"#7D5B52\"}]","template":[{"type": "text", "params": {"src": "Cafe", "editorMode": true, "draggable": true, "x": 257, "y": 111, "textAlign": "center", "font": "Kaushan Script", "colors": ["#7D5B52"], "textSize": 35}},{"type": "image", "params": {"src": "assets/img/sample/cafe.png","editorMode": true, "draggable": true, "resizeToW": 185, "x": 143, "y": 151}},{"type": "text", "params": {"src": "Coffee,Tea", "editorMode": true, "draggable": true, "x": 174, "y": 299, "textAlign": "center", "font": "Dancing Script", "colors": ["#7D5B52"], "textSize": 35,"fontWeight":"bold"}}]}'></div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-03/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-03/partials/right-base.png\", \"x\": 72, \"y\": 88, \"z\": 1,\"color\": \"#85A35D\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-03/partials/right-handle.png\", \"x\": 267, \"y\": 92, \"z\": 2,\"color\": \"#7D5B52\"}]","template":[{"type": "text", "params": {"src": "KEEP", "editorMode": true, "draggable": true, "x": 133, "y": 120, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 25}},{"type": "text", "params": {"src": "CALM", "editorMode": true, "draggable": true, "x": 129, "y": 142, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 25}},{"type": "text", "params": {"src": "AND", "editorMode": true, "draggable": true, "x": 148, "y": 165, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 15}},{"type": "text", "params": {"src": "DRINK", "editorMode": true, "draggable": true, "x": 125, "y": 182, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 25}},{"type": "text", "params": {"src": "COFFEE", "editorMode": true, "draggable": true, "x": 122, "y": 209, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 20}},{"type": "image", "params": {"src": "assets/img/sample/coffee.png","editorMode": true, "draggable": true, "resizeToW": 94, "x": 118, "y": 233}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-04/mug-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-04/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-04/partials/left-base.png\", \"x\": 71, \"y\": 102, \"z\": 1,\"color\": \"#EDB963\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-04/partials/left-handle.png\", \"x\": 38, \"y\": 147, \"z\": 2,\"color\": \"#ECE5DA\"},{\"name\": \"upper-portion\", \"src\": \"assets/img/products/mug-04/partials/left-upper.png\", \"x\": 73, \"y\": 75, \"z\": 3,\"color\": \"#ECE5DA\"}]","template":[{"type": "text", "params": {"src": "Vintage", "editorMode": true, "draggable": true, "x": 231, "y": 133, "textAlign": "center", "font": "Dancing Script", "colors": ["#D1316F"], "textSize": 45}},{"type": "text", "params": {"src": "Looking good", "editorMode": true, "draggable": true, "x": 248, "y": 184, "textAlign": "center", "font": "Dancing Script","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 17}},{"type": "text", "params": {"src": "SINCE", "editorMode": true, "draggable": true, "x": 267, "y": 206, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 15}},{"type": "text", "params": {"src": "1999", "editorMode": true, "draggable": true, "x": 261, "y": 220, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 25}},{"type": "image", "params": {"src": "assets/img/sample/swan.png","editorMode": true, "draggable": true, "resizeToW": 140, "x": 101, "y":136 }}]}'></div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-04/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-04/partials/right-base.png\", \"x\": 36, \"y\": 99, \"z\": 1,\"color\": \"#EDB963\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-04/partials/right-handle.png\", \"x\": 309, \"y\": 143, \"z\": 2,\"color\": \"#ECE5DA\"},{\"name\": \"upper-portion\", \"src\": \"assets/img/products/mug-04/partials/right-upper.png\", \"x\": 36, \"y\": 73, \"z\": 3,\"color\": \"#ECE5DA\"}]","template":[{"type": "text", "params": {"src": "VINTAGE", "editorMode": true, "draggable": true, "x": 66, "y": 133, "textAlign": "center", "font": "Architects Daughter","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 30}},{"type": "text", "params": {"src": "made in", "editorMode": true, "draggable": true, "x": 79, "y": 160, "textAlign": "center", "font": "Dancing Script","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 25}},{"type": "text", "params": {"src": "1990", "editorMode": true, "draggable": true, "x": 75, "y": 185, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 35}},{"type": "image", "params": {"src": "assets/img/sample/art.png","editorMode": true, "draggable": true, "resizeToW": 184, "x": 125, "y":122 }}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-05/mug-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-05/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-05/partials/left-base.png\", \"x\": 70, \"y\": 107, \"z\": 1,\"color\": \"#C8D8AB\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-05/partials/left-handle.png\", \"x\": 35, \"y\": 143, \"z\": 2,\"color\": \"#E7DBCB\"},{\"name\": \"upper-portion\", \"src\": \"assets/img/products/mug-05/partials/left-upper.png\", \"x\": 71, \"y\": 73, \"z\": 3,\"color\": \"#E7DBCB\"}]","template":[{"type": "text", "params": {"src": "SPECIALLY BREWED FOR", "editorMode": true, "draggable": true, "x": 140, "y": 195, "textAlign": "center", "font": "Black Ops One", "colors": ["#D1316F"], "textSize": 12}},{"type": "text", "params": {"src": "KAROLINA", "editorMode": true, "draggable": true, "x": 146, "y": 208, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#FED348"], "textSize": 28,"stroke":"#000000"}},{"type": "image", "params": {"src": "assets/img/sample/decoration2.png","editorMode": true, "draggable": true, "resizeToW": 290, "x": 71, "y": 131}},{"type": "image", "params": {"src": "assets/img/sample/decoration2.png","editorMode": true, "draggable": true, "resizeToW": 196, "x": 119, "y": 249}}]}'></div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-05/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-05/partials/right-base.png\", \"x\": 35, \"y\": 108, \"z\": 1,\"color\": \"#C8D8AB\"},{\"name\": \"handle\", \"src\": \"assets/img/products/mug-05/partials/right-handle.png\", \"x\": 309, \"y\":143, \"z\": 2,\"color\": \"#E7DBCB\"},{\"name\": \"upper-portion\", \"src\": \"assets/img/products/mug-05/partials/right-upper.png\", \"x\": 37, \"y\": 73, \"z\": 3,\"color\": \"#E7DBCB\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/specs.png","editorMode": true, "draggable": true, "resizeToW": 256, "x": 55, "y": 135,"degree":6}},{"type": "text", "params": {"src": "nerds", "editorMode": true, "draggable": true, "x":73, "y": 218, "textAlign": "center", "font": "Dancing Script","fontWeight":"bold", "colors": ["#304972"], "textSize": 25}},{"type": "text", "params": {"src": "are just", "editorMode": true, "draggable": true, "x": 128, "y": 233, "textAlign": "center", "font": "Dancing Script","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 25}},{"type": "text", "params": {"src": "cooler", "editorMode": true, "draggable": true, "x": 210, "y": 242, "textAlign": "center", "font": "Dancing Script","fontWeight":"bold", "colors": ["#304972"], "textSize": 25}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
									</div>
								</div>
								<!--/ Products -->

								<!-- <br>
								<input type="number" class="form-control input-lg" id="path-left" placeholder="Left">
								<br>
								<input type="number" class="form-control input-lg" id="path-top" placeholder="Top">
								<br>
								<input type="number" class="form-control input-lg" id="path-height" placeholder="Height"> -->

								<!-- Text Pane -->
								<div role="tabpanel" class="tab-pane fade text-center" id="tabpane-text-options">
									<p class="custom-pane-header text-center h3">Text Options</p><hr>
									<br/>
									<textarea class="form-control" id="text-content"></textarea>
									<br/>
									<select class="form-control input-lg" id="text-font-family">
										<option style="font-family:Arial" value="Arial">Arial</option>
										<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
										<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
										<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
										<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
										<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
										<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
										<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
										<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
										<option style="font-family:Chewy" value="Chewy">Chewy</option>
										<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>	
									</select>
									<br>
									<button id="btn-add-text" type="button" class="btn btn-default btn-lg">Add Text</button>
								</div>
								<!--/ Text Pane -->
								
								<!-- Image Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-image-options">
									<p class="custom-pane-header text-center h3">Image Options</p><hr>
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
										<li><a role="presentation" href="#uploads" aria-controls="uploads" role="tab" data-toggle="tab">Uploads</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="gallery">
											<div class="row display-gallery">
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/nike.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/adidas.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/being-human.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/puma.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/tattoos-01.png" alt=""></div>
												
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/camera-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/dragon-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/hearts-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/happy-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/sad-01.png" alt=""></div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="uploads">
											<div class="row display-gallery">
												<div class="col-sm-24 text-center">
													<br>
													<button id="btn-add-img" type="button" class="btn btn-default btn-lg"><i class="ion-ios-upload-outline"></i> Upload Image</button>
													<input type="file" id="add-img" style="display: none;" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Image Pane -->
								
								<!-- Cart Calculations Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-price-calculation-options">
									<p class="custom-pane-header text-center h3">Price Calculations</p><hr>
									<div class="row">
										<div class="col-xs-12 col-sm-16 col-md-24">
											<!-- table -->
											<table class="table table-hover">
												<thead>
													<tr>
														<th>Size</th>
														<th class="text-center">#</th>
														<th class="text-center">Total</th>
													</tr>
												</thead>
												<tbody>
													<tr data-toggle="popover" data-popover-size="xs" title="Name & Number" data-html="true" data-content="<div data-nn-size='xs'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>XS</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="xs" class="sizes xs_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="xs_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="s" title="Name & Number" data-html="true" data-content="<div data-nn-size='s'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>S</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="s" class="sizes s_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="s_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="m" title="Name & Number" data-html="true" data-content="<div data-nn-size='m'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>M</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="m" class="sizes m_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="m_total">0</span></td>
													</tr>												
													<tr data-toggle="popover" data-popover-size="l" title="Name & Number" data-html="true" data-content="<div data-nn-size='l'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>L</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="l" class="sizes l_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="l_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="xl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>XL</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="xl" class="sizes xl_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="xl_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="xxl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xxl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>XXL</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="xxl" class="sizes xxl_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="xxl_total">0</span></td>
													</tr>
													<tr>
														<td></td>													
														<td class="text-center">
															<h4><strong>Total</strong></h4>
														</td>
														<td class="text-center text-danger">
															<h4><strong>$<span class="total_price">0.00</span></strong></h4>	
														</td>
													</tr>
												</tbody>
											</table>
											<!--/ table -->
											<button class="btn btn-lg btn-block btn-default">Add To Cart</button>
										</div>
									</div>
								</div>
								<!--/ Cart Calculations Pane -->
							</div>
							<!--/ Tab Panes -->
							<br>
						</div>
						<div class="col-sm-18 col-md-12 popup-options">
							<span class="visible-md visible-lg"><br><br><br><br></span>
							<span class="visible-sm"><hr></span>
							<div class="popup-text-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-fill">
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Stroke</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-stroke">
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="" style="background-color: "></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-group btn-group-justified" role="group" id="text-properties">
									<div class="btn-group" role="group">
										<button type="button" id="text-bold" class="btn btn-default"><i class="fa fa-bold"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-italic" class="btn btn-default"><i class="fa fa-italic"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-underline" class="btn btn-default"><i class="fa fa-underline"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-line-through" class="btn btn-default"><i class="fa fa-strikethrough"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-overline" class="btn btn-default"><img style="width:14px;height:14px" src="assets/img/control-icons/text-overline.svg" alt="O"></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-left" class="btn btn-default"><i class="fa fa-align-left"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-center" class="btn btn-default"><i class="fa fa-align-center"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-right"  class="btn btn-default"><i class="fa fa-align-right"></i></button>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-12">
										<b>Height:</b><br><br><div id="text-line-height"></div>
									</div>
									<div class="col-xs-12">
										<b>Opacity:</b><br><br><div id="text-opacity"></div>
									</div>
								</div>
							</div>
							<div class="popup-image-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="image-fill">
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#000000" style="background-color: #000000"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="popup-product-options hide">
								<div class="row display-gallery base-colors">
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#663D50" style="background-color: #663D50"></div></div>
								</div>
							</div>
						</div>
					</div>
					<!--/ Left -->

					<div class="col-sm-14 col-md-14">
						<div class="row">
							<!-- Center -->
							<div class="col-sm-14 col-md-18">
								<!-- Design Tool Stage-->
								<div class="col-xs-24">
									<div class="text-center stage" id="design-studio">
										<div data-side="front" data-params='{"preview": "assets/img/products/mug-02/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-02/partials/left-base.png\", \"x\": 76, \"y\": 92, \"z\": 1,\"color\": \"#E7DBCB\"}]","template":[{"type": "text", "params": {"src": "You are", "editorMode": true, "draggable": true, "x": 171, "y": 151, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#1E3C62"], "textSize": 25}},{"type": "text", "params": {"src": "the", "editorMode": true, "draggable": true, "x": 179, "y": 181, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#6D4D40"], "textSize": 18}},{"type": "text", "params": {"src": "Best", "editorMode": true, "draggable": true, "x": 216, "y": 178, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#1F9DA9"], "textSize": 25}},{"type": "text", "params": {"src": "MUM", "editorMode": true, "draggable": true, "x": 176, "y": 201, "textAlign": "center", "font": "Quicksand","fontWeight":"bold", "colors": ["#F58383"], "textSize": 38}},{"type": "image", "params": {"src": "assets/img/sample/flags.png","editorMode": true, "draggable": true, "resizeToW": 194, "x": 126, "y": 93}}]}'></div>
										<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-02/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/mug-02/partials/right-base.png\", \"x\": 76, \"y\": 92, \"z\": 1,\"color\": \"#E7DBCB\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/flags.png","editorMode": true, "draggable": true, "resizeToW": 186, "x": 80, "y": 93}},{"type": "text", "params": {"src": "A   AZING", "editorMode": true, "draggable": true, "x": 122, "y": 150, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "M", "editorMode": true, "draggable": true, "x": 136, "y": 150, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "L   VELY", "editorMode": true, "draggable": true, "x": 122, "y": 170, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "O", "editorMode": true, "draggable": true, "x": 136, "y": 170, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "IN   ELLIJENT", "editorMode": true, "draggable": true, "x": 113, "y": 190, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "T", "editorMode": true, "draggable": true, "x": 136, "y": 190, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "H", "editorMode": true, "draggable": true, "x": 136, "y": 210, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "APPY", "editorMode": true, "draggable": true, "x": 151, "y": 210, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "B   AUTIFUL", "editorMode": true, "draggable": true, "x": 119, "y": 230, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "E", "editorMode": true, "draggable": true, "x": 136, "y": 230, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}},{"type": "text", "params": {"src": "MA   VELOUS", "editorMode": true, "draggable": true, "x": 105, "y": 250, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#304972"], "textSize": 20}},{"type": "text", "params": {"src": "R", "editorMode": true, "draggable": true, "x": 137, "y": 250, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#D1316F"], "textSize": 20}}]}'></div>
									</div>
									<!-- <div class="col-sm-20 col-sm-offset-2">
										<br><br>
										<div id="zoom-stage"></div>
										<br><br><br><br>
									</div> -->
								</div>
								<!--/ Design Tool Stage-->							
							</div>
							<!--/ Center -->

							<!-- Right -->
							<div class="col-sm-10 col-md-6">
								<div class="row">
									<!-- Preview Images -->
									<div class="col-sm-12 text-center preview-images">
										<div class="row">								
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="front-preview" alt="Front">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="left-preview" alt="Left">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="back-preview" alt="Back">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="right-preview" alt="Right">
											</div>
										</div>
										<!-- <hr>
										<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div> -->
									</div>
									<!--/ Preview Images -->
								</div>
							</div>
							<!--/ Right -->							
						</div>

						<div class="row">
							<div class="col-sm-14 col-md-18">
								<div class="col-sm-20 col-sm-offset-2">
									<br>
									<div id="zoom-stage"></div>
								</div>
							</div>

							<!-- Price -->
							<div class="col-sm-10 col-md-6">
								<span class="visible-xs"><br><br><br></span>
								<div class="col-sm-12 text-center total_price_col">
									<br>
									<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
								</div>
							</div>
							<!--/ Price -->
						</div>
					</div>
				</div>
				<!--/ Design Tool -->
			</div>
		</div>
		<!--/  Main Container -->
		<section class="preload-fonts"></section>

		<!-- JS -->
		<!-- plugins -->
		<script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/lib/confirmation/bootstrap-confirmation.min.js"></script>
		<script type="text/javascript" src="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>
		<script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>
		
		<script type="text/javascript" src="assets/lib/nouislider/nouislider.min.js"></script>
		<!--/ plugins -->
		<script type="text/javascript" src="assets/js/index.js"></script>
		<!--/ JS -->
	</body>
</html>