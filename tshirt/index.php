<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DS Advance - Tshirt</title>
		<!-- CSS -->

		<!-- plugins -->
		<link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Josefin+Sans|Shadows+Into+Light|Pacifico|Quicksand|Architects+Daughter|Dancing+Script|Chewy|Gloria+Hallelujah|Shadows+Into+Light|Poiret+One|Indie+Flower|Oswald|Trebuchet+MS|Russo+One|Black+Ops+One|Kaushan+Script' rel='stylesheet' type='text/css'>
	
		<link rel="stylesheet"  href="assets/lib/nouislider/nouislider.min.css" media="all" type="text/css" />
		<!--/ plugins -->
		
		<link rel="stylesheet" href="assets/css/menu.css">
		<link rel="stylesheet" href="assets/css/media.css">
		<link rel="stylesheet" href="assets/css/index.css">
		<!--/ CSS -->
	</head>
	<body>
		<div id="loader">
			<p class="text-center"><br>Loading.... Please wait...</p>
		</div>
		<!-- header -->
		<header>
			<!-- Nav -->
			<nav class="animate focus">
				<ul class="nav nav-pills nav-stacked tool-options" role="tablist">
					<li role="presentation" class="active"><a class="text-center" title="Products" data-toggle="tab" aria-controls="tabpane-products" href="#tabpane-products"><i class="fa fa-th"></i></a></li>
					<li role="presentation"><a class="text-center" title="Text Options" data-toggle="tab" aria-controls="tabpane-text-options" href="#tabpane-text-options"><i class="fa fa-pencil"></i></a></li>
					<li role="presentation"><a class="text-center" title="Image Options" data-toggle="tab" aria-controls="tabpane-image-options" href="#tabpane-image-options"><i class="fa fa-picture-o"></i></a></li>
					<li role="presentation"><a class="text-center" title="Price Calculation" data-toggle="tab" aria-controls="tabpane-price-calculation-options" href="#tabpane-price-calculation-options"><i class="fa fa-cart-plus"></i></a></li>
				</ul>
			</nav>
			<!--/ Nav -->
			<div class="nav-controller hidden">
				<span class="[ ion-2x ion-navicon-round ] controller-open"></span>
				<span class="[ ion-2x ion-ios-close-outline ] controller-close"></span>
			</div>
		</header>
		<!--/ header -->		

		<!--  Main Container -->
		<div class="content-wrapper container-fluid">
			<div class="row">	

				<!-- Tools -->
				<div class="row tools">
					<div class="col-sm-8 col-md-10 col-sm-offset-10 col-md-offset-10">
						<div class="btn-group btn-group-justified" role="group" aria-label="">
							<div class="btn-group" role="group">
								<button title="Reset" type="button" data-toggle="confirmation" class="btn btn-default" id="reset"><i class="ion-2x ion-ios-loop"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Flip Horizontally" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Horizontally" class="btn btn-default" id="flipX"><i class="ion-2x ion-ios-arrow-thin-right"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Flip Vertically" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Vertically" class="btn btn-default" id="flipY"><i class="ion-2x ion-ios-arrow-thin-up"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button title="Save" type="button" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-default" id="saveProduct"><i class="ion-2x ion-ios-heart-outline"></i></i></button>
							</div>
						</div>
					</div>
				</div>
				<!--/ Tools -->

				<!-- Design Tool -->
				<div class="row">

					<!-- Left -->
					<div class="col-sm-10 col-md-10">
						<div class="col-sm-18 col-md-12">
							<!-- Tab Panes -->
							<div class="tab-content tool-panes">
								<!-- Products -->
								<div role="tabpanel" class="products tab-pane fade in active" id="tabpane-products">
									<p class="custom-pane-header text-center h3">Products</p><hr>
									<div class="row display-gallery products-list">
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/tshirt-02/front-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-02/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/front-base.png\", \"x\": 75, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\",\"resizeToW\":238}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/front-collar.png\", \"x\": 154, \"y\": 18,  \"z\": 2,\"color\":\"#304972\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/front-left-arm.png\", \"x\": 295, \"y\": 51,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/front-left-cuff.png\", \"x\": 310, \"y\": 183,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":63}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/front-right-arm.png\", \"x\": 32, \"y\": 66,  \"z\": 5,\"color\":\"#C2BEBF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/front-right-cuff.png\", \"x\": 30, \"y\": 183,  \"z\": 6,\"color\":\"#304972\",\"resizeToW\":69}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/front-hinge.png\", \"x\": 93, \"y\": 352,  \"z\": 7,\"color\":\"#304972\",\"resizeToW\":215}]", "template": [{"type": "text", "params": {"src": "KIEHLS", "editorMode": true, "draggable": true, "x": 126, "y": 85, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 45}}, {"type": "image", "params": {"src": "assets/img/sample/bullet.png","editorMode": true, "draggable": true, "resizeToW": 157, "x": 113, "y": 106}},{"type": "text", "params": {"src": "PRODUCTS", "editorMode": true, "draggable": true, "x": 118, "y": 287, "textAlign": "center", "font": "Trebuchet MS", "fontWeight":"bold","colors": ["#304972"], "textSize": 12}},{"type": "text", "params": {"src": "for the", "editorMode": true, "draggable": true, "x": 186, "y": 286,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#304972"], "textSize": 13}},{"type": "text", "params": {"src": "RUGGED", "editorMode": true, "draggable": true, "x": 225, "y": 286, "textAlign": "center", "font": "trebuchet MS", "fontWeight":"bold","colors": ["#304972"], "textSize": 12}},{"type": "text", "params": {"src": "NEW YORK APOTHECARY", "editorMode": true, "draggable": true, "x": 118, "y": 302, "textAlign": "center", "font": "trebuchet MS", "colors": ["#304972"],"fontWeight":"bold", "textSize": 14}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 167, "y": 317, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 11,"fontWeight":"bold"}}]}'></div>


												<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-02/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/left-base.png\", \"x\": 123, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/left-collar.png\", \"x\": 172, \"y\": 18,  \"z\": 2,\"color\":\"#304972\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/left-left-arm.png\", \"x\": 188, \"y\": 56,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/left-left-cuff.png\", \"x\": 192, \"y\": 155,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":75}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/left-hinge.png\", \"x\": 124, \"y\": 360,  \"z\": 5,\"color\":\"#304972\",\"resizeToW\":146}]","template":[{"type": "text", "params": {"src": "Kiehls", "editorMode": true, "draggable": true, "x": 201, "y": 117, "textAlign": "center", "font": "Dancing Script", "colors": ["#304972"], "textSize": 22,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 206, "y": 141, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 8,"fontWeight":"bold"}}]}'></div>
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-02/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/back-base.png\", \"x\": 74, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/back-collar.png\", \"x\": 154, \"y\": 17,  \"z\": 2,\"color\":\"#304972\",\"resizeToW\":86}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/back-left-arm.png\", \"x\": 31, \"y\": 66,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/back-left-cuff.png\", \"x\": 30, \"y\": 183,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":68}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/back-right-arm.png\", \"x\": 295, \"y\": 51,  \"z\": 5,\"color\":\"#C2BEBF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/back-right-cuff.png\", \"x\": 309, \"y\": 184,  \"z\": 6,\"color\":\"#304972\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/back-hinge.png\", \"x\": 94, \"y\": 353,  \"z\": 7,\"color\":\"#304972\",\"resizeToW\":215}]","template":[{"type": "text", "params": {"src": "KIEHLS", "editorMode": true, "draggable": true, "x": 126, "y": 85, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 45}}, {"type": "text", "params": {"src": "A NEW YORK ", "editorMode": true, "draggable": true, "x": 109, "y": 193, "textAlign": "center", "font": "trebuchet MS", "colors": ["#304972"],"fontWeight":"bold", "textSize": 10}},{"type": "image", "params": {"src": "assets/img/sample/log.png","editorMode": true, "draggable": true, "resizeToW": 180, "x": 109, "y": 152,"z":8}},{"type": "text", "params": {"src": " KIEHLS", "editorMode": true, "draggable": true, "x": 181, "y": 159, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FFFFFF"],"fontWeight":"bold", "textSize": 9}},{"type": "text", "params": {"src": " DE CALIDAD", "editorMode": true, "draggable": true, "x": 133, "y": 162, "textAlign": "center","degree":25, "font": "Trebuchet MS", "colors": ["#FFFFFF"],"fontWeight":"bold", "textSize": 8}},{"type": "text", "params": {"src": " Y SERVICIO", "editorMode": true, "draggable": true, "x": 221, "y": 182, "textAlign": "center","degree":335, "font": "Trebuchet MS", "colors": ["#FFFFFF"],"fontWeight":"bold", "textSize": 8}},{"type": "text", "params": {"src": " APOTHECARY", "editorMode": true, "draggable": true, "x": 232, "y": 193, "textAlign": "center", "font": "trebuchet MS", "colors": ["#304972"],"fontWeight":"bold", "textSize": 10}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 167, "y": 135, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 11,"fontWeight":"bold"}}]}'></div>

												<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-02/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/right-base.png\", \"x\": 123, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/right-collar.png\", \"x\": 164, \"y\": 18,  \"z\": 2,\"color\":\"#304972\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/right-right-arm.png\", \"x\": 131, \"y\": 56,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/right-right-cuff.png\", \"x\": 134, \"y\": 155,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":76}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/right-hinge.png\", \"x\": 128, \"y\": 360,  \"z\": 5,\"color\":\"#304972\",\"resizeToW\":151}]","template":[{"type": "text", "params": {"src": "Kiehls", "editorMode": true, "draggable": true, "x": 148, "y": 116, "textAlign": "center", "font": "Dancing Script", "colors": ["#304972"], "textSize": 22,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 153, "y":142, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 8,"fontWeight":"bold"}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/tshirt-02/front-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-02/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/front-base.png\", \"x\": 75, \"y\": 20, \"z\": 1,\"color\": \"#C8D8AB\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/front-collar.png\", \"x\": 154, \"y\": 18,  \"z\": 2,\"color\":\"#FED348\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/front-left-arm.png\", \"x\": 295, \"y\": 51,  \"z\": 3,\"color\":\"#FFFFFF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/front-left-cuff.png\", \"x\": 310, \"y\": 183,  \"z\": 4,\"color\":\"#FED348\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/front-right-arm.png\", \"x\": 31, \"y\": 66,  \"z\": 5,\"color\":\"#FFFFFF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/front-right-cuff.png\", \"x\": 29, \"y\": 183,  \"z\": 6,\"color\":\"#FED348\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/front-hinge.png\", \"x\": 96, \"y\": 353,  \"z\": 7,\"color\":\"#FED348\",\"resizeToW\":208}]", "template": [{"type": "text", "params": {"src": "ONE ", "editorMode": true, "draggable": true, "x": 134, "y": 169, "textAlign": "center", "font": "Arial", "colors": ["#02BBFF"], "textSize": 30,"fontWeight":"bold","degree":270,"stroke":"#28272C"}},{"type": "image", "params": {"src": "assets/img/sample/flash.png","editorMode": true, "draggable": true, "resizeToW": 32, "x": 164, "y": 126}},{"type": "text", "params": {"src": "DAY ", "editorMode": true, "draggable": true, "x": 166, "y": 102, "textAlign": "center", "font": "Arial", "colors": ["#D1316F"], "textSize": 23,"fontWeight":"bold"}},{"type": "text", "params": {"src": "O\nU\n R ", "editorMode": true, "draggable": true, "x": 199, "y": 120, "textAlign": "center", "font": "Arial", "colors": ["#D1316F"], "textSize": 15,"fontWeight":"bold"}},{"type": "text", "params": {"src": "LIFE ", "editorMode": true, "draggable": true, "x": 217, "y": 137, "textAlign": "center", "font": "Arial", "colors": ["#05B8F9"], "textSize": 15,"fontWeight":"bold"}},{"type": "text", "params": {"src": "WILL ", "editorMode": true, "draggable": true, "x": 217, "y": 154, "textAlign": "center", "font": "Arial", "colors": ["#02BBFF"], "textSize": 15,"fontWeight":"bold"}},{"type": "text", "params": {"src": "FLASH ", "editorMode": true, "draggable": true, "x": 142, "y": 166, "textAlign": "center", "font": "Arial", "colors": ["#FFFC00"], "textSize": 35,"fontWeight":"bold","stroke":"#28272C"}},{"type": "text", "params": {"src": "BEFORE YOUR EYES ", "editorMode": true, "draggable": true, "x": 144, "y": 198, "textAlign": "center", "font": "Arial", "colors": ["#B3042B"], "textSize": 11,"fontWeight":"bold"}},{"type": "text", "params": {"src": "MAKE ", "editorMode": true, "draggable": true, "x": 146, "y": 209, "textAlign": "center", "font": "Arial", "colors": ["#FF0000"], "textSize": 20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SURE ", "editorMode": true, "draggable": true, "x": 212, "y": 211, "textAlign": "center", "font": "Arial", "colors": ["#B3042B"], "textSize": 8,"fontWeight":"bold"}},{"type": "text", "params": {"src": "ITS ", "editorMode": true, "draggable": true, "x": 212, "y": 219, "textAlign": "center", "font": "Arial", "colors": ["#B3042B"], "textSize": 8,"fontWeight":"bold"}},{"type": "text", "params": {"src": "WORTH ", "editorMode": true, "draggable": true, "x": 186, "y": 225, "textAlign": "center", "font": "Arial", "colors": ["#B3042B"], "textSize": 20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "WATCHING ", "editorMode": true, "draggable": true, "x": 154, "y": 244, "textAlign": "center", "font": "Arial", "colors": ["#02BBFF"], "textSize": 20,"fontWeight":"bold","stroke":"#28272C"}}]}'></div>

												<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-02/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/left-base.png\", \"x\": 121, \"y\": 20, \"z\": 1,\"color\": \"#C8D8AB\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/left-collar.png\", \"x\": 172, \"y\": 18,  \"z\": 2,\"color\":\"#FED348\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/left-left-arm.png\", \"x\": 186, \"y\": 56,  \"z\": 3,\"color\":\"#FFFFFF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/left-left-cuff.png\", \"x\": 192, \"y\": 155,  \"z\": 4,\"color\":\"#FED348\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/left-hinge.png\", \"x\": 124, \"y\": 360,  \"z\": 5,\"color\":\"#FED348\",\"resizeToW\":148}]","template":[{"type": "image", "params": {"src": "assets/img/sample/wolves.png","editorMode": true, "draggable": true, "resizeToW": 67, "x": 193, "y": 106}},{"type": "text", "params": {"src": "Flash Wolves ", "editorMode": true, "draggable": true, "x": 193, "y": 140, "textAlign": "center", "font": "Architects Daughter", "colors": ["#D1316F"], "textSize": 10,"fontWeight":"bold"}}]}'></div>

												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-02/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/back-base.png\", \"x\": 74, \"y\": 20, \"z\": 1,\"color\": \"#C8D8AB\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/back-collar.png\", \"x\": 153, \"y\": 17,  \"z\": 2,\"color\":\"#FED348\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/back-left-arm.png\", \"x\": 31, \"y\": 66,  \"z\": 3,\"color\":\"#FFFFFF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/back-left-cuff.png\", \"x\": 30, \"y\": 183,  \"z\": 4,\"color\":\"#FED348\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/back-right-arm.png\", \"x\": 294, \"y\": 51,  \"z\": 5,\"color\":\"#FFFFFF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/back-right-cuff.png\", \"x\": 308, \"y\": 182,  \"z\": 6,\"color\":\"#FED348\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/back-hinge.png\", \"x\": 96, \"y\": 353,  \"z\": 7,\"color\":\"#FED348\",\"resizeToW\":208}]","template":[{"type": "image", "params": {"src": "assets/img/sample/wolves.png","editorMode": true, "draggable": true, "resizeToW": 153, "x": 123, "y": 88}},{"type": "text", "params": {"src": "Flash Wolves ", "editorMode": true, "draggable": true, "x": 126, "y": 165, "textAlign": "center", "font": "Kaushan Script", "colors": ["#304972"], "textSize": 25,"fontWeight":"bold","stroke":"#E975A4"}}]}'></div>

												<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-02/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/right-base.png\", \"x\": 121, \"y\": 20, \"z\": 1,\"color\": \"#C8D8AB\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/right-collar.png\", \"x\": 163, \"y\": 18,  \"z\": 2,\"color\":\"#FED348\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/right-right-arm.png\", \"x\": 129, \"y\": 56,  \"z\": 3,\"color\":\"#FFFFFF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/right-right-cuff.png\", \"x\": 134, \"y\": 155,  \"z\": 4,\"color\":\"#FED348\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/right-hinge.png\", \"x\": 130, \"y\": 360,  \"z\": 5,\"color\":\"#FED348\",\"resizeToW\":148}]","template":[{"type": "image", "params": {"src": "assets/img/sample/wolves.png","editorMode": true, "draggable": true, "resizeToW": 66, "x": 134, "y": 109}},{"type": "text", "params": {"src": "Flash Wolves ", "editorMode": true, "draggable": true, "x": 136, "y": 141, "textAlign": "center", "font": "Architects Daughter", "colors": ["#D1316F"], "textSize": 10,"fontWeight":"bold"}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/tshirt-03/front-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-03/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/front-base.png\", \"x\": 78, \"y\": 40, \"z\": 1}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/front-collar.png\", \"x\": 145, \"y\": 11,  \"z\": 2}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-03/partials/front-left-arm.png\", \"x\": 292, \"y\": 64,  \"z\": 3}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/front-left-cuff.png\", \"x\": 304, \"y\": 189,  \"z\": 4}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-03/partials/front-right-arm.png\", \"x\": 36, \"y\": 79,  \"z\": 5}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/front-right-cuff.png\", \"x\": 35, \"y\": 191,  \"z\": 6}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/front-hinge.png\", \"x\": 100, \"y\": 356,  \"z\": 7}]", "template": [{"type": "text", "params": {"src": "SPARTACUS", "editorMode": true, "draggable": true, "x": 144, "y": 166, "textAlign": "center", "font": "Shadows Into Light","fontWeight":"bold", "colors": ["#7D5B52"], "textSize": 25}},{"type": "text", "params": {"src": "BLOOD AND SAND", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 146, "y": 199, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#7D5B52"], "textSize": 15}}, {"type": "image", "params": {"src": "assets/img/sample/spark.png","editorMode": true, "draggable": true, "resizeToW": 180, "x": 102, "y": 214}}]}'></div>

												<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-03/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/left-base.png\", \"x\": 130, \"y\": 32, \"z\": 1}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/left-collar.png\", \"x\": 159, \"y\": 10,  \"z\": 2}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-03/partials/left-left-arm.png\", \"x\": 171, \"y\": 66,  \"z\": 3}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/left-left-cuff.png\", \"x\": 172, \"y\": 153,  \"z\": 4}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/left-hinge.png\", \"x\": 135, \"y\": 376,  \"z\": 5}]","template":[{"type": "image", "params": {"src": "assets/img/sample/logo.png","editorMode": true, "draggable": true, "resizeToW": 61, "x":178, "y": 120,"degree":15}},{"type": "text", "params": {"src": "S", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 199, "y": 131, "textAlign": "center", "font": "Arial", "colors": ["#01858A"], "textSize": 13,"degree":13}}]}'></div>

												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-03/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/back-base.png\", \"x\": 78, \"y\": 36, \"z\": 1}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/back-collar.png\", \"x\": 150, \"y\": 10,  \"z\": 2}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-03/partials/back-right-arm.png\", \"x\": 292, \"y\": 64,  \"z\": 3}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/back-right-cuff.png\", \"x\": 304, \"y\": 189,  \"z\": 4}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-03/partials/back-left-arm.png\", \"x\": 36, \"y\": 79,  \"z\": 5}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/back-left-cuff.png\", \"x\": 35, \"y\": 191,  \"z\": 6}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/back-hinge.png\", \"x\": 100, \"y\": 356,  \"z\": 7}]","template": [ {"type": "image", "params": {"src": "assets/img/sample/logo4.png","editorMode": true, "draggable": true, "resizeToW": 180, "x": 110, "y": 91}},{"type": "text", "params": {"src": "SPARTACUS", "editorMode": true, "draggable": true, "x": 135, "y": 180, "textAlign": "center", "font": "Shadows Into Light","fontWeight":"bold", "colors": ["#7D5B52"], "textSize": 30}},{"type": "text", "params": {"src": "VENGEANCE", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 155, "y": 215, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#7D5B52"], "textSize": 20}}]}'></div>

												<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-03/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/right-base.png\", \"x\": 130, \"y\": 32, \"z\": 1}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/right-collar.png\", \"x\": 162, \"y\": 9,  \"z\": 2}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-03/partials/right-right-arm.png\", \"x\": 155, \"y\": 66,  \"z\": 3}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/right-right-cuff.png\", \"x\": 168, \"y\": 153,  \"z\": 4}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/right-hinge.png\", \"x\": 130, \"y\": 375,  \"z\": 5}]","template":[{"type": "image", "params": {"src": "assets/img/sample/logo.png","editorMode": true, "draggable": true, "resizeToW": 56, "x":171, "y": 140,"degree":333}},{"type": "text", "params": {"src": "S", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 193, "y": 136, "textAlign": "center", "font": "Arial", "colors": ["#01858A"], "textSize": 13,"degree":336}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/tshirt-03/front-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-03/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/front-base.png\", \"x\": 78, \"y\": 40, \"z\": 1,\"color\": \"#EDB963\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/front-collar.png\", \"x\": 145, \"y\": 11,  \"z\": 2,\"color\": \"#C2BEBF\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-03/partials/front-left-arm.png\", \"x\": 292, \"y\": 64,  \"z\": 3,\"color\": \"#EDB963\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/front-left-cuff.png\", \"x\": 304, \"y\": 189,  \"z\": 4,\"color\": \"#C2BEBF\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-03/partials/front-right-arm.png\", \"x\": 36, \"y\": 79,  \"z\": 5,\"color\": \"#EDB963\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/front-right-cuff.png\", \"x\": 35, \"y\": 191,  \"z\": 6,\"color\": \"#C2BEBF\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/front-hinge.png\", \"x\": 100, \"y\": 356,  \"z\": 7,\"color\": \"#EDB963\"}]", "template": [{"type": "text", "params": {"src": "ROCKDALE COUNTRY HIGH SCHOOL", "editorMode": true, "draggable": true, "x":221, "y": 160, "textAlign": "center", "font": "Shadows Into Light","fontWeight":"bold", "colors": ["#B3042B"], "textSize": 13,"degree":89}},{"type": "text", "params": {"src": "CLASS OF", "editorMode": true, "draggable": true, "x":225, "y": 158, "textAlign": "center", "font": "Trebuchet MS","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 8}},{"type": "text", "params": {"src": "09", "editorMode": true, "draggable": true, "x":226, "y": 163, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#891C2F"], "textSize": 30,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SENIORS", "editorMode": true, "draggable": true, "x":262, "y": 194, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 35,"degree":89,"stroke":"#000000"}}]}'></div>

												<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-03/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/left-base.png\", \"x\": 130, \"y\": 32, \"z\": 1,\"color\": \"#EDB963\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/left-collar.png\", \"x\": 159, \"y\": 10,  \"z\": 2,\"color\": \"#C2BEBF\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-03/partials/left-left-arm.png\", \"x\": 171, \"y\": 66,  \"z\": 3,\"color\": \"#EDB963\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/left-left-cuff.png\", \"x\": 172, \"y\": 153,  \"z\": 4,\"color\": \"#C2BEBF\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/left-hinge.png\", \"x\": 135, \"y\": 376,  \"z\": 5,\"color\":\"#EDB963\"}]","template":[{"type": "text", "params": {"src": "R", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 192, "y": 118, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#B3042B"], "textSize": 30}},{"type": "text", "params": {"src": "C", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 199, "y": 127, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#B3042B"], "textSize":30}},{"type": "text", "params": {"src": "CLASS OF", "editorMode": true, "draggable": true, "x":159, "y": 184, "textAlign": "center", "font": "Trebuchet MS","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 8}},{"type": "text", "params": {"src": "09", "editorMode": true, "draggable": true, "x":161, "y": 191, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#891C2F"], "textSize": 25,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SENIORS", "editorMode": true, "draggable": true, "x":191, "y": 216, "textAlign": "center", "font": "Arial","fontWeight":"bold", "colors": ["#891C2F"], "textSize": 30,"degree":89,"stroke":"#000000"}}]}'></div>

												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-03/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/back-base.png\", \"x\": 78, \"y\": 36, \"z\": 1,\"color\": \"#EDB963\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/back-collar.png\", \"x\": 150, \"y\": 10,  \"z\": 2,\"color\": \"#C2BEBF\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-03/partials/back-right-arm.png\", \"x\": 292, \"y\": 64,  \"z\": 3,\"color\": \"#EDB963\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/back-right-cuff.png\", \"x\": 304, \"y\": 189,  \"z\": 4,\"color\": \"#C2BEBF\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-03/partials/back-left-arm.png\", \"x\": 36, \"y\": 79,  \"z\": 5,\"color\": \"#EDB963\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/back-left-cuff.png\", \"x\": 35, \"y\": 191,  \"z\": 6,\"color\": \"#C2BEBF\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/back-hinge.png\", \"x\": 100, \"y\": 356,  \"z\": 7,\"color\": \"#EDB963\"}]","template": [ {"type": "text", "params": {"src": "OUT OF THE DAWGHOUSE", "editorMode": true, "draggable": true, "x": 135, "y": 90, "textAlign": "center", "font": "Shadows Into Light","fontWeight":"bold", "colors": ["#B3042B"], "textSize": 12}},{"type": "text", "params": {"src": "CLASS OF", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 135, "y": 145, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#B3042B"], "textSize": 8,"degree":271}},{"type": "text", "params": {"src": "09", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 144, "y": 143, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#B3042B"], "textSize": 29,"degree":270}},{"type": "text", "params": {"src": "SENIORS", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 174, "y": 117, "textAlign": "center", "font": "Arial", "colors": ["#B3042B"], "textSize": 20}},{"type": "text", "params": {"src": "R", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 160, "y": 151, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#D1316F"], "textSize": 45}},{"type": "text", "params": {"src": "C", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 167, "y": 159, "textAlign": "center", "font": "Architects Daughter", "colors": ["#4A5D7B"], "textSize": 45}},{"type": "text", "params": {"src": "H", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 205, "y": 156, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#7D5B52"], "textSize": 45}},{"type": "text", "params": {"src": "S", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 213, "y": 139, "textAlign": "center", "font": "Architects Daughter", "colors": ["#00995B"], "textSize": 45}}]}'></div>

												<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-03/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-03/partials/right-base.png\", \"x\": 130, \"y\": 32, \"z\": 1,\"color\": \"#EDB963\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-03/partials/right-collar.png\", \"x\": 162, \"y\": 9,  \"z\": 2,\"color\": \"#C2BEBF\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-03/partials/right-right-arm.png\", \"x\": 155, \"y\": 66,  \"z\": 3,\"color\": \"#EDB963\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-03/partials/right-right-cuff.png\", \"x\": 168, \"y\": 153,  \"z\": 4,\"color\": \"#C2BEBF\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-03/partials/right-hinge.png\", \"x\": 130, \"y\": 375,  \"z\": 5,\"color\": \"#EDB963\"}]","template":[{"type": "text", "params": {"src": "R", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 182, "y": 121, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#B3042B"], "textSize": 30}},{"type": "text", "params": {"src": "C", "editorMode": true, "draggable": true,"fontWeight":"bold", "x": 189, "y": 127, "textAlign": "center", "font": "Shadows Into Light", "colors": ["#B3042B"], "textSize":30}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/tshirt-04/front-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-04/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/front-base.png\", \"x\": 78, \"y\": 18, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/front-collar.png\", \"x\": 131, \"y\": 20,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-04/partials/front-left-arm.png\", \"x\": 287, \"y\": 44,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/front-left-cuff.png\", \"x\": 290, \"y\": 150,  \"z\": 4,\"color\": \"#663D50\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-04/partials/front-right-arm.png\", \"x\": 56, \"y\": 58,  \"z\": 5,\"color\": \"#FEAC1C\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/front-right-cuff.png\", \"x\": 55, \"y\": 167,  \"z\": 6,\"color\": \"#663D50\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/jesus.png","editorMode": true, "draggable": true, "resizeToW": 100, "x":138, "y": 83}},{"type": "text", "params": {"src": "J", "editorMode": true, "draggable": true, "x": 137, "y": 223, "textAlign": "center", "font": "Black Ops One", "colors": ["#FEAC1C"], "textSize": 50}},{"type": "text", "params": {"src": "ESUS", "editorMode": true, "draggable": true, "x": 172, "y": 240, "textAlign": "center", "font": "Black Ops One", "colors": ["#FEAC1C"], "textSize": 30}},{"type": "text", "params": {"src": "IS LORD", "editorMode": true, "draggable": true, "x": 154, "y": 269, "textAlign": "center", "font": "Black Ops One", "colors": ["#FEAC1C"], "textSize":22}}]}'></div>

												<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-04/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/left-base.png\", \"x\": 112, \"y\": 14, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/left-collar.png\", \"x\": 146, \"y\": 15,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-04/partials/left-left-arm.png\", \"x\": 181, \"y\": 36,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/left-left-cuff.png\", \"x\": 211, \"y\": 144,  \"z\": 4,\"color\": \"#663D50\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/jesus1.png","editorMode": true, "draggable": true, "resizeToW": 30, "x":228, "y": 112}}]}'></div>

												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-04/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/back-base.png\", \"x\": 95, \"y\": 15, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/back-collar.png\", \"x\": 154, \"y\": 17,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-04/partials/back-right-arm.png\", \"x\": 294, \"y\": 49,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/back-right-cuff.png\", \"x\": 295, \"y\": 153,  \"z\": 4,\"color\": \"#663D50\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-04/partials/back-left-arm.png\", \"x\": 54, \"y\": 41,  \"z\": 5,\"color\": \"#FEAC1C\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/back-left-cuff.png\", \"x\": 52, \"y\": 141,  \"z\": 6,\"color\": \"#663D50\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/jesus1.png","editorMode": true, "draggable": true, "resizeToW": 98, "x":141, "y": 55}},{"type": "text", "params": {"src": "He who believes", "editorMode": true, "draggable": true, "x": 136, "y": 173, "textAlign": "center", "font": "Dancing Script", "colors": ["#FEAC1C"], "textSize":20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "in Me has", "editorMode": true, "draggable": true, "x": 153, "y": 195, "textAlign": "center", "font": "Dancing Script", "colors": ["#FEAC1C"], "textSize":20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "Everlasting Life", "editorMode": true, "draggable": true, "x": 133, "y": 217, "textAlign": "center", "font": "Dancing Script", "colors": ["#FEAC1C"], "textSize":20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "-Jesus", "editorMode": true, "draggable": true, "x": 206, "y": 246, "textAlign": "center", "font": "Dancing Script", "colors": ["#FEAC1C"], "textSize":20,"fontWeight":"bold"}}]}'></div>

												<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-04/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/right-base.png\", \"x\": 154, \"y\": 14, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/right-collar.png\", \"x\": 178, \"y\": 14,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-04/partials/right-right-arm.png\", \"x\": 114, \"y\": 36,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/right-right-cuff.png\", \"x\": 115, \"y\": 145,  \"z\": 4,\"color\": \"#663D50\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/jesus1.png","editorMode": true, "draggable": true, "resizeToW": 29, "x":144, "y": 114,"degree":2}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/tshirt-04/front-thumbnail.png" alt="" width="369" height="430">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-04/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/front-base.png\", \"x\": 78, \"y\": 18, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/front-collar.png\", \"x\": 131, \"y\": 20,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-04/partials/front-left-arm.png\", \"x\": 287, \"y\": 44,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/front-left-cuff.png\", \"x\": 290, \"y\": 150,  \"z\": 4,\"color\": \"#663D50\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-04/partials/front-right-arm.png\", \"x\": 56, \"y\": 58,  \"z\": 5,\"color\": \"#FEAC1C\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/front-right-cuff.png\", \"x\": 55, \"y\": 167,  \"z\": 6,\"color\": \"#663D50\"}]","template":[{"type": "text", "params": {"src": "WHEN", "editorMode": true, "draggable": true, "x": 138, "y": 96, "textAlign": "center", "font": "Architects Daughter", "colors": ["#FEAC1C"], "textSize": 20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "LIFE GETS", "editorMode": true, "draggable": true, "x": 140, "y": 118, "textAlign": "center", "font": "Architects Daughter", "colors": ["#FEAC1C"], "textSize": 20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "COMPLICATED", "editorMode": true, "draggable": true, "x": 135, "y": 142, "textAlign": "center", "font": "Architects Daughter", "colors": ["#FEAC1C"], "textSize": 15,"fontWeight":"bold"}},{"type": "text", "params": {"src": "JUST", "editorMode": true, "draggable": true, "x": 164, "y": 162, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FEAC1C"], "textSize": 20,"fontWeight":"bold"}},{"type": "text", "params": {"src": "RIDE", "editorMode": true, "draggable": true, "x": 166, "y": 182, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FEAC1C"], "textSize": 20,"fontWeight":"bold"}},{"type": "image", "params": {"src": "assets/img/sample/ride.png","editorMode": true, "draggable": true, "resizeToW": 182, "x":102, "y": 189}}]}'></div>

												<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-04/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/left-base.png\", \"x\": 112, \"y\": 14, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/left-collar.png\", \"x\": 146, \"y\": 15,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-04/partials/left-left-arm.png\", \"x\": 181, \"y\": 36,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/left-left-cuff.png\", \"x\": 211, \"y\": 144,  \"z\": 4,\"color\": \"#663D50\"}]","template":[{"type": "text", "params": {"src": "Riding", "editorMode": true, "draggable": true, "x": 216, "y": 135, "textAlign": "center", "font": "Dancing Script", "colors": ["#E975A4"], "textSize":20,"fontWeight":"bold","degree":344}}]}'></div>

												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-04/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/back-base.png\", \"x\": 95, \"y\": 15, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/back-collar.png\", \"x\": 154, \"y\": 17,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-04/partials/back-right-arm.png\", \"x\": 294, \"y\": 49,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/back-right-cuff.png\", \"x\": 295, \"y\": 153,  \"z\": 4,\"color\": \"#663D50\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-04/partials/back-left-arm.png\", \"x\": 54, \"y\": 41,  \"z\": 5,\"color\": \"#FEAC1C\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/back-left-cuff.png\", \"x\": 52, \"y\": 141,  \"z\": 6,\"color\": \"#663D50\"}]","template":[{"type": "image", "params": {"src": "assets/img/sample/ride1.png","editorMode": true, "draggable": true, "resizeToW": 182, "x":96, "y": 141}},{"type": "text", "params": {"src": "Born", "editorMode": true, "draggable": true, "x": 160, "y": 70, "textAlign": "center", "font": "Dancing Script", "colors": ["#FEAC1C"], "textSize":30,"fontWeight":"bold"}},{"type": "text", "params": {"src": "to Ride", "editorMode": true, "draggable": true, "x": 161, "y": 99, "textAlign": "center", "font": "Dancing Script", "colors": ["#FEAC1C"], "textSize":30,"fontWeight":"bold"}}]}'></div>

												<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-04/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-04/partials/right-base.png\", \"x\": 154, \"y\": 14, \"z\": 1,\"color\": \"#663D50\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-04/partials/right-collar.png\", \"x\": 178, \"y\": 14,  \"z\": 2,\"color\": \"#FEAC1C\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-04/partials/right-right-arm.png\", \"x\": 114, \"y\": 36,  \"z\": 3,\"color\": \"#FEAC1C\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-04/partials/right-right-cuff.png\", \"x\": 115, \"y\": 145,  \"z\": 4,\"color\": \"#663D50\"}]","template":[{"type": "text", "params": {"src": "Riding", "editorMode": true, "draggable": true, "x": 134, "y": 122, "textAlign": "center", "font": "Dancing Script", "colors": ["#E975A4"], "textSize":20,"fontWeight":"bold","degree":9}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
									</div>
								</div>
								<!--/ Products -->

								<!-- <br>
								<input type="number" class="form-control input-lg" id="path-left" placeholder="Left">
								<br>
								<input type="number" class="form-control input-lg" id="path-top" placeholder="Top">
								<br>
								<input type="number" class="form-control input-lg" id="path-height" placeholder="Height"> -->

								<!-- Text Pane -->
								<div role="tabpanel" class="tab-pane fade text-center" id="tabpane-text-options">
									<p class="custom-pane-header text-center h3">Text Options</p><hr>
									<br/>
									<textarea class="form-control" id="text-content"></textarea>
									<br/>
									<select class="form-control input-lg" id="text-font-family">
										<option style="font-family:Arial" value="Arial">Arial</option>
										<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
										<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
										<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
										<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
										<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
										<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
										<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
										<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
										<option style="font-family:Chewy" value="Chewy">Chewy</option>
										<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>	
									</select>
									<br>
									<button id="btn-add-text" type="button" class="btn btn-default btn-lg">Add Text</button>
								</div>
								<!--/ Text Pane -->
								
								<!-- Image Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-image-options">
									<p class="custom-pane-header text-center h3">Image Options</p><hr>
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
										<li><a role="presentation" href="#uploads" aria-controls="uploads" role="tab" data-toggle="tab">Uploads</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="gallery">
											<div class="row display-gallery">
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/nike.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/adidas.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/being-human.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/puma.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/tattoos-01.png" alt=""></div>
												
												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/camera-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/dragon-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/hearts-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/happy-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" src="assets/img/sample/sad-01.png" alt=""></div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="uploads">
											<div class="row display-gallery">
												<div class="col-sm-24 text-center">
													<br>
													<button id="btn-add-img" type="button" class="btn btn-default btn-lg"><i class="ion-ios-upload-outline"></i> Upload Image</button>
													<input type="file" id="add-img" style="display: none;" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Image Pane -->
								
								<!-- Cart Calculations Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-price-calculation-options">
									<p class="custom-pane-header text-center h3">Price Calculations</p><hr>
									<div class="row">
										<div class="col-xs-12 col-sm-16 col-md-24">
											<!-- table -->
											<table class="table table-hover">
												<thead>
													<tr>
														<th>Size</th>
														<th class="text-center">#</th>
														<th class="text-center">Total</th>
													</tr>
												</thead>
												<tbody>
													<tr data-toggle="popover" data-popover-size="xs" title="Name & Number" data-html="true" data-content="<div data-nn-size='xs'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>XS</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="xs" class="sizes xs_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="xs_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="s" title="Name & Number" data-html="true" data-content="<div data-nn-size='s'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>S</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="s" class="sizes s_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="s_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="m" title="Name & Number" data-html="true" data-content="<div data-nn-size='m'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>M</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="m" class="sizes m_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="m_total">0</span></td>
													</tr>												
													<tr data-toggle="popover" data-popover-size="l" title="Name & Number" data-html="true" data-content="<div data-nn-size='l'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>L</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="l" class="sizes l_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="l_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="xl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>XL</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="xl" class="sizes xl_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="xl_total">0</span></td>
													</tr>
													<tr data-toggle="popover" data-popover-size="xxl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xxl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
														<td class="col-sm-6"><em>XXL</em></td>
														<td class="col-sm-2 text-center"><input type="text" data-input-size="xxl" class="sizes xxl_count form-control input-sm"></td>
														<td class="col-sm-2 text-center">$<span class="xxl_total">0</span></td>
													</tr>
													<tr>
														<td></td>													
														<td class="text-center">
															<h4><strong>Total</strong></h4>
														</td>
														<td class="text-center text-danger">
															<h4><strong>$<span class="total_price">0.00</span></strong></h4>	
														</td>
													</tr>
												</tbody>
											</table>
											<!--/ table -->
											<button class="btn btn-lg btn-block btn-default">Add To Cart</button>
										</div>
									</div>
								</div>
								<!--/ Cart Calculations Pane -->
							</div>
							<!--/ Tab Panes -->
							<br>
						</div>
						<div class="col-sm-18 col-md-12 popup-options">
							<span class="visible-md visible-lg"><br><br><br><br></span>
							<span class="visible-sm"><hr></span>
							<div class="popup-text-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-fill">
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Stroke</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-stroke">
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="" style="background-color: "></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-stroke="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-group btn-group-justified" role="group" id="text-properties">
									<div class="btn-group" role="group">
										<button type="button" id="text-bold" class="btn btn-default"><i class="fa fa-bold"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-italic" class="btn btn-default"><i class="fa fa-italic"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-underline" class="btn btn-default"><i class="fa fa-underline"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-line-through" class="btn btn-default"><i class="fa fa-strikethrough"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-overline" class="btn btn-default"><img style="width:14px;height:14px" src="assets/img/control-icons/text-overline.svg" alt="O"></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-left" class="btn btn-default"><i class="fa fa-align-left"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-center" class="btn btn-default"><i class="fa fa-align-center"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-right"  class="btn btn-default"><i class="fa fa-align-right"></i></button>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-12">
										<b>Height:</b><br><br><div id="text-line-height"></div>
									</div>
									<div class="col-xs-12">
										<b>Opacity:</b><br><br><div id="text-opacity"></div>
									</div>
								</div>
							</div>
							<div class="popup-image-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="image-fill">
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#000000" style="background-color: #000000"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="popup-product-options hide">
								<div class="row display-gallery base-colors">
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#008767" style="background-color: #008767"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#304972" style="background-color: #304972"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#304050" style="background-color: #304050"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-xs-8 col-md-6"><div class="well" data-color="#663D50" style="background-color: #663D50"></div></div>
								</div>
							</div>
						</div>
					</div>
					<!--/ Left -->

					<div class="col-sm-14 col-md-14">
						<div class="row">
							<!-- Center -->
							<div class="col-sm-14 col-md-18">
								<!-- Design Tool Stage-->
								<div class="col-xs-24">
									<div class="text-center stage" id="design-studio">
										<div data-side="front" data-params='{"preview": "assets/img/products/tshirt-02/front-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/front-base.png\", \"x\": 75, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\",\"resizeToW\":238}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/front-collar.png\", \"x\": 154, \"y\": 18,  \"z\": 2,\"color\":\"#304972\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/front-left-arm.png\", \"x\": 295, \"y\": 51,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/front-left-cuff.png\", \"x\": 310, \"y\": 183,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":63}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/front-right-arm.png\", \"x\": 32, \"y\": 66,  \"z\": 5,\"color\":\"#C2BEBF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/front-right-cuff.png\", \"x\": 30, \"y\": 183,  \"z\": 6,\"color\":\"#304972\",\"resizeToW\":69}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/front-hinge.png\", \"x\": 93, \"y\": 352,  \"z\": 7,\"color\":\"#304972\",\"resizeToW\":215}]", "template": [{"type": "text", "params": {"src": "KIEHLS", "editorMode": true, "draggable": true, "x": 126, "y": 85, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 45}}, {"type": "image", "params": {"src": "assets/img/sample/bullet.png","editorMode": true, "draggable": true, "resizeToW": 157, "x": 113, "y": 106}},{"type": "text", "params": {"src": "PRODUCTS", "editorMode": true, "draggable": true, "x": 118, "y": 287, "textAlign": "center", "font": "Trebuchet MS", "fontWeight":"bold","colors": ["#304972"], "textSize": 12}},{"type": "text", "params": {"src": "for the", "editorMode": true, "draggable": true, "x": 186, "y": 286,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#304972"], "textSize": 13}},{"type": "text", "params": {"src": "RUGGED", "editorMode": true, "draggable": true, "x": 225, "y": 286, "textAlign": "center", "font": "trebuchet MS", "fontWeight":"bold","colors": ["#304972"], "textSize": 12}},{"type": "text", "params": {"src": "NEW YORK APOTHECARY", "editorMode": true, "draggable": true, "x": 118, "y": 302, "textAlign": "center", "font": "trebuchet MS", "colors": ["#304972"],"fontWeight":"bold", "textSize": 14}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 167, "y": 317, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 11,"fontWeight":"bold"}}]}'></div>
										<div class="hide" data-side="left" data-params='{"preview": "assets/img/products/tshirt-02/left-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/left-base.png\", \"x\": 123, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/left-collar.png\", \"x\": 172, \"y\": 18,  \"z\": 2,\"color\":\"#304972\"}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/left-left-arm.png\", \"x\": 188, \"y\": 56,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/left-left-cuff.png\", \"x\": 192, \"y\": 155,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":75}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/left-hinge.png\", \"x\": 124, \"y\": 360,  \"z\": 5,\"color\":\"#304972\",\"resizeToW\":146}]","template":[{"type": "text", "params": {"src": "Kiehls", "editorMode": true, "draggable": true, "x": 201, "y": 117, "textAlign": "center", "font": "Dancing Script", "colors": ["#304972"], "textSize": 22,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 206, "y": 141, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 8,"fontWeight":"bold"}}]}'></div>

										<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-02/back-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/back-base.png\", \"x\": 74, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/back-collar.png\", \"x\": 154, \"y\": 17,  \"z\": 2,\"color\":\"#304972\",\"resizeToW\":86}, {\"name\": \"left-arm\", \"src\": \"assets/img/products/tshirt-02/partials/back-left-arm.png\", \"x\": 31, \"y\": 66,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"left-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/back-left-cuff.png\", \"x\": 30, \"y\": 183,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":68}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/back-right-arm.png\", \"x\": 295, \"y\": 51,  \"z\": 5,\"color\":\"#C2BEBF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/back-right-cuff.png\", \"x\": 309, \"y\": 184,  \"z\": 6,\"color\":\"#304972\"}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/back-hinge.png\", \"x\": 94, \"y\": 353,  \"z\": 7,\"color\":\"#304972\",\"resizeToW\":215}]","template":[{"type": "text", "params": {"src": "KIEHLS", "editorMode": true, "draggable": true, "x": 126, "y": 85, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 45}}, {"type": "text", "params": {"src": "A NEW YORK ", "editorMode": true, "draggable": true, "x": 109, "y": 193, "textAlign": "center", "font": "trebuchet MS", "colors": ["#304972"],"fontWeight":"bold", "textSize": 10}},{"type": "image", "params": {"src": "assets/img/sample/log.png","editorMode": true, "draggable": true, "resizeToW": 180, "x": 109, "y": 152,"z":8}},{"type": "text", "params": {"src": " KIEHLS", "editorMode": true, "draggable": true, "x": 181, "y": 159, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FFFFFF"],"fontWeight":"bold", "textSize": 9}},{"type": "text", "params": {"src": " DE CALIDAD", "editorMode": true, "draggable": true, "x": 133, "y": 162, "textAlign": "center","degree":25, "font": "Trebuchet MS", "colors": ["#FFFFFF"],"fontWeight":"bold", "textSize": 8}},{"type": "text", "params": {"src": " Y SERVICIO", "editorMode": true, "draggable": true, "x": 221, "y": 182, "textAlign": "center","degree":335, "font": "Trebuchet MS", "colors": ["#FFFFFF"],"fontWeight":"bold", "textSize": 8}},{"type": "text", "params": {"src": " APOTHECARY", "editorMode": true, "draggable": true, "x": 232, "y": 193, "textAlign": "center", "font": "trebuchet MS", "colors": ["#304972"],"fontWeight":"bold", "textSize": 10}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 167, "y": 135, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 11,"fontWeight":"bold"}}]}'></div>

										<div class="hide" data-side="right" data-params='{"preview": "assets/img/products/tshirt-02/right-preview.png", "bases": "[{\"name\": \"base\", \"src\": \"assets/img/products/tshirt-02/partials/right-base.png\", \"x\": 123, \"y\": 20, \"z\": 1,\"color\": \"#C2BEBF\"}, {\"name\": \"collar\", \"src\": \"assets/img/products/tshirt-02/partials/right-collar.png\", \"x\": 164, \"y\": 18,  \"z\": 2,\"color\":\"#304972\"}, {\"name\": \"right-arm\", \"src\": \"assets/img/products/tshirt-02/partials/right-right-arm.png\", \"x\": 131, \"y\": 56,  \"z\": 3,\"color\":\"#C2BEBF\"}, {\"name\": \"right-cuff\", \"src\": \"assets/img/products/tshirt-02/partials/right-right-cuff.png\", \"x\": 134, \"y\": 155,  \"z\": 4,\"color\":\"#304972\",\"resizeToW\":76}, {\"name\": \"hinge\", \"src\": \"assets/img/products/tshirt-02/partials/right-hinge.png\", \"x\": 128, \"y\": 360,  \"z\": 5,\"color\":\"#304972\",\"resizeToW\":151}]","template":[{"type": "text", "params": {"src": "Kiehls", "editorMode": true, "draggable": true, "x": 148, "y": 116, "textAlign": "center", "font": "Dancing Script", "colors": ["#304972"], "textSize": 22,"fontWeight":"bold"}},{"type": "text", "params": {"src": "SINCE 1851", "editorMode": true, "draggable": true, "x": 153, "y":142, "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 8,"fontWeight":"bold"}}]}'></div>
									</div>
									<!-- <div class="col-sm-20 col-sm-offset-2">
										<br><br>
										<div id="zoom-stage"></div>
										<br><br><br><br>
									</div> -->
								</div>
								<!--/ Design Tool Stage-->							
							</div>
							<!--/ Center -->

							<!-- Right -->
							<div class="col-sm-10 col-md-6">
								<div class="row">
									<!-- Preview Images -->
									<div class="col-sm-12 text-center preview-images">
										<div class="row">								
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="front-preview" alt="Front">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="left-preview" alt="Left">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="back-preview" alt="Back">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="right-preview" alt="Right">
											</div>
										</div>
										<!-- <hr>
										<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div> -->
									</div>
									<!--/ Preview Images -->
								</div>
							</div>
							<!--/ Right -->							
						</div>

						<div class="row">
							<div class="col-sm-14 col-md-18">
								<div class="col-sm-20 col-sm-offset-2">
									<br>
									<div id="zoom-stage"></div>
								</div>
							</div>

							<!-- Price -->
							<div class="col-sm-10 col-md-6">
								<span class="visible-xs"><br><br><br></span>
								<div class="col-sm-12 text-center total_price_col">
									<br>
									<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
								</div>
							</div>
							<!--/ Price -->
						</div>
					</div>
				</div>
				<!--/ Design Tool -->
			</div>
		</div>
		<!--/  Main Container -->
		<section class="preload-fonts"></section>

		<!-- JS -->
		<!-- plugins -->
		<script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/lib/confirmation/bootstrap-confirmation.min.js"></script>
		<script type="text/javascript" src="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>
		<script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>
		
		<script type="text/javascript" src="assets/lib/nouislider/nouislider.min.js"></script>
		<!--/ plugins -->
		<script type="text/javascript" src="assets/js/index.js"></script>
		<!--/ JS -->
	</body>
</html>